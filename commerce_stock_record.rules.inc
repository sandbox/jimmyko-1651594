<?php

/**
 * @file
 * Rules integration for Commerce Stock Record.
 */


function rules_commerce_stock_record_integration_access($type, $name) {
  if ($type == 'event' || $type == 'condition') {
    return entity_access('view', 'commerce_stock_record');
  }
}

/**
 * Implements hook_rules_event_info().
 */
function commerce_stock_record_rules_event_info() {
  $defaults = array(
    'group' => t('Commerce Stock Record'),
    'module' => 'commerce_stock_record',
    'access callback' => 'rules_commerce_stock_record_integration_access',
  );
  return array(
    'commerce_stock_record_adjust_multiple_prepare' => $defaults + array(
      'label' => t('Prepare the list of commerce stock record for multiple adjustment'),
      'variables' => array(
        'stock_records' => array(
          'type' => 'list<entity>', 
          'label' => t('List of stock records.')
        ),
      ),
    ),
    'commerce_stock_record_after_adjust' => $defaults + array(
      'label' => t('After updating an existing commerce stock record with extra information'),
      'variables' => array(
        'stock_record' => array(
          'type' => 'commerce_stock_record',
          'label' => t('Stock record updated.'),
        ),
        'adjustment' => array(
          'type' => 'decimal',
          'label' => t('The amount changed of stock level.'),
        ),
        'extra' => array(
          'type' => 'list<entity>',
          'label' => t('An associative array of fields information.'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function commerce_stock_record_rules_condition_info() {
  $defaults = array(
    'group' => t('Commerce Stock Record'),
    'access callback' => 'rules_commerce_stock_record_integration_access',
  );
  $conditions['stock_record_enabled_on_product'] = $defaults + array(
    'label' => t('Product has stock record enabled for its type'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_stock_record_product_enabled',
    ),
  );
  $conditions['stock_record_exist_for_product'] = $defaults + array(
    'label' => t('Stock record for that product is existing.'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_stock_record_product_stock_record_exist',
    ),
  );
  $conditions['stock_record_of_product_is_out_of_stock'] = $defaults + array(
    'label' => t('Product which is out of stock'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_stock_record_product_is_out_of_stock',
    ),
  );
  $conditions['stock_record_of_product_is_enough'] = $defaults + array(
    'label' => t('Product which has enough stock level'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
      'qty' => array(
        'type' => 'decimal',
        'label' => t('Quantity'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_stock_record_product_is_enough',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_stock_record_rules_action_info() {
  $defaults = array(
    'group' => t('Commerce Stock Record'),
    'access callback' => 'rules_commerce_stock_record_integration_access',
  );
  $actions['commerce_stock_record_adjust'] = $defaults + array(
    'label' => t('Adjust stock record value.'),
    'parameter' => array(
      'commerce_stock_record' => array(
        'type' => 'commerce_stock_record',
        'label' => t('Stock record'),
      ),
      'qty' => array(
        'type' => 'decimal',
        'label' => t('Quantity'),
        'default value' => 0,
      ),
    ),
  );
  $actions['commerce_stock_record_adjust_by_stock_transaction'] = $defaults + array(
    'label' => t('Adjust stock record value, given a stock transaction'),
    'parameter' => array(
      'commerce_stock_transaction' => array(
        'type' => 'commerce_stock_transaction',
        'label' => t('Stock transaction'),
      ),
    ),
  );
  $actions['commerce_stock_record_decrease_by_line_item'] = $defaults + array(
    'label' => t('Decrease stock record, given a line item'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Stock line item'),
      ),
    ),
  );
  $actions['commerce_stock_record_load_by_product'] = $defaults + array(
    'label' => t('Load product\'s stock record(s) by product.'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
  );

  return $actions;
}
