<?php

/**
 * Add new commerce stock record page callback.
 */
function commerce_stock_record_add($sku) {
  $stock_record = entity_create('commerce_stock_record', array());
  drupal_set_title(t('Create Stock Record'));
  if (!empty($sku)) {
    $stock_record->product_sku = $sku;
  }
  $output = drupal_get_form('commerce_stock_record_form', $stock_record);
  return $output;
}

/**
 * Stock record Form.
 */
function commerce_stock_record_form($form, &$form_state, $stock_record) {
  $form_state['stock_record'] = $stock_record;

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $stock_record->uid,
  );

  $form['product_sku'] = array(
    '#type' => 'textfield',
    '#title' => t('Product SKU'),
    '#description' => t('Enter the SKU of the product'),
    '#autocomplete_path' => 'commerce_product/autocomplete/commerce_stock_record/product_sku/commerce_stock_record',
    '#required' => TRUE,
    '#size' => 60,
    '#default_value' => $stock_record->product_sku,
    '#maxlength' => 255,
  );

  field_attach_form('commerce_stock_record', $stock_record, $form, $form_state);

  $form['stock'] = array(
    '#type' => 'textfield',
    '#default_value' => $stock_record->stock,
    '#title' => t('Stock'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $stock_record->description,
  );

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save stock record'),
    '#submit' => $submit + array('commerce_stock_record_form_submit'),
  );

  // Show Delete button if we edit stock record.
  $stock_record_id = entity_id('commerce_stock_record' ,$stock_record);
  if (!empty($stock_record_id) && commerce_stock_record_access('edit', $stock_record)) {
    drupal_set_title(t('Stock Record (Stock ID: !id)', array('!id' => $stock_record_id)));

    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('commerce_stock_record_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'commerce_stock_record_form_validate';

  return $form;
}

function commerce_stock_record_form_validate($form, &$form_state) {

}

/**
 * Stock record submit handler.
 */
function commerce_stock_record_form_submit($form, &$form_state) {
  $stock_record = $form_state['stock_record'];
  entity_form_submit_build_entity('commerce_stock_record', $stock_record, $form, $form_state);
  commerce_stock_record_save($stock_record);
  //$stock_record_uri = entity_uri('commerce_stock_record', $stock_record);
  // $form_state['redirect'] = $stock_record_uri['path'];
  $form_state['redirect'] = 'admin/commerce/stock';
  drupal_set_message(t('Stock record %title saved.', array('%title' => entity_label('commerce_stock_record', $stock_record))));
}

function commerce_stock_record_form_submit_delete($form, &$form_state) {
  $stock_record = $form_state['stock_record'];
  $stock_record_uri = entity_uri('commerce_stock_record', $stock_record);

  // drupal_goto() always favors $_GET['destination'] over its own $path parameter
  // so we reset the $_GET['destination'] and clear cache here
  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();

  $form_state['redirect'] = $stock_record_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function commerce_stock_record_delete_form($form, &$form_state, $stock_record) {
  $form_state['stock_record'] = $stock_record;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['stock_type_id'] = array('#type' => 'value', '#value' => entity_id('commerce_stock_record', $stock_record));
  $stock_record_uri = entity_uri('commerce_stock_record', $stock_record);
  return confirm_form($form,
    t('Are you sure you want to delete stock record %title?', array('%title' => entity_label('commerce_stock_record', $stock_record))),
    $stock_record_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function commerce_stock_record_delete_form_submit($form, &$form_state) {
  $stock_record = $form_state['stock_record'];
  commerce_stock_record_delete($stock_record);

  drupal_set_message(t('Stock record %title deleted.', array('%title' => entity_label('commerce_stock_record', $stock_record))));

  $form_state['redirect'] = 'admin/commerce/stock';
}

/**
 * Configuration form
 */
function commerce_stock_record_admin_config_form($form, &$form_state) {
  $types = commerce_product_types();
  $type_options = array();
  foreach($types as $key=>$val) {
    $type_options[$key] = $types[$key]['name'];
  }

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stock Record Settings'),
  );
  $form['settings']['commerce_stock_record_product_type_enable'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Commerce Product Types'),
    '#description' => t('Check to enable stock record for the product type.'),
    '#options' => $type_options,
    '#default_value' => variable_get('commerce_stock_record_product_type_enable', array()),
  );
  $form['settings']['commerce_stock_record_multivalue'] = array(
    '#type' => 'checkbox',
    '#title' => t('Multiple Values'),
    '#description' => t('Toggle this option to enable/disable multiple stock records for each product.'),
    '#default_value' => variable_get('commerce_stock_record_multivalue', 0),
  );
  $form['settings']['commerce_stock_record_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Threshold value'),
    '#description' => t('User can set threshold for stock level to act as a limiter/buffer.'),
    '#default_value' => variable_get('commerce_stock_record_threshold', 0),
    '#size' => 5,
    '#element_validate' => array('_commerce_stock_record_threshold_validate'),
  );
  return system_settings_form($form);
}


/**
 * Element validate function for threshold element.
 *
 * @see commerce_stock_record_admin_config_form().
 */
function _commerce_stock_record_threshold_validate($element, &$form_state, $form) {
  if (!commerce_stock_record_threshold_validate($element['#value'])) {
    form_error($element, t('Invalid threshold value'));
  }
}


/**
 * Check if threshold is valid.
 *
 * @param $value
 *   Number: Threshold value.
 * @return
 *   Boolean: TRUE if the threshold value is valid.
 */
function commerce_stock_record_threshold_validate($value) {
  if (is_null($value) || !is_numeric($value) || $value<0) {
    return FALSE;
  }
  return TRUE;
}
