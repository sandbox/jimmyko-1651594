<?php

class CommerceStockRecordController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'product_sku' => '',
      'stock' => 0,
      'description' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('commerce_stock_record', $entity);
    $extra_fields = field_info_extra_fields('commerce_stock_record', 'commerce_stock_record', 'display');

    $content['author'] = array(
      '#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))),
      '#weight' => $extra_fields['author']['display'][$view_mode]['weight'],
    );

    $product_link = '';
    if (!empty($entity->product_sku)) {
      $product = commerce_product_load_by_sku($entity->product_sku);
      $product_uri = ltrim(commerce_product_get_properties($product, array(), 'edit_url'), base_path());
      $product_link = l($entity->product_sku, $product_uri);
    }
    $content['product_sku'] = array(
      '#entity_type' => 'commerce_stock_record',
      '#bundle' => 'commerce_stock_record',
      '#field_name' => 'field_stock_record_product_sku',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Product'),
      '#items' => array(array('value' => $entity->product_sku)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['product_sku']['display'][$view_mode]['weight'],
      0 => array('#markup' => $product_link)
    );

    $content['stock'] = array(
      '#entity_type' => 'commerce_stock_record',
      '#bundle' => 'commerce_stock_record',
      '#field_name' => 'field_stock_record_stock',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Stock'),
      '#items' => array(array('value' => $entity->stock)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['stock']['display'][$view_mode]['weight'],
      0 => array('#markup' => check_plain($entity->stock))
    );

    // Make Description and Status themed like default fields.
    $content['description'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Description'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'field_stock_record_description',
      '#field_type' => 'text',
      '#entity_type' => 'commerce_stock_record',
      '#bundle' => 'commerce_stock_record',
      '#items' => array(array('value' => $entity->description)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['description']['display'][$view_mode]['weight'],
      0 => array('#markup' => check_plain($entity->description))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

/**
 * Commerce Stock Record class.
 */
class CommerceStockRecord extends Entity {
  protected function defaultLabel() {
    return $this->stkid;
  }

  protected function defaultUri() {
    return array('path' => 'admin/commerce/stock/' . $this->identifier());
  }

  public function save() {
    $this->changed = REQUEST_TIME;
    return parent::save();
  }
}

/**
 * Commerce Stock Record Views Controller class.
 */
class CommerceStockRecordViewsController extends EntityDefaultViewsController {
  public function views_data() {
    $data = array();
    $data = parent::views_data();
    $data['commerce_stock_record']['product_sku']['field']['handler'] = 'commerce_stock_record_views_handler_field_product_sku';
    $data['commerce_stock_record']['product_sku']['relationship'] = array(
      'label' => t('Product'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_product',
      'base field' => 'sku',
      'relationship field' => 'product_sku',
    );
    return $data;
  }
}


/**
 * Commerce Stock Record Views Product SKU field handler class.
 */
class commerce_stock_record_views_handler_field_product_sku extends views_handler_field {
  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  public function render($values) {
    $output = '';
    $sku = $values->commerce_stock_record_product_sku;
    if (!empty($sku)) {
      $product = commerce_product_load_by_sku($sku);
      $product_uri = ltrim(commerce_product_get_properties($product, array(), 'edit_url'), base_path());
      $output = l($sku, $product_uri);
    }
    return $output;
  }
  
  function document_self_tokens(&$tokens) {
    parent::document_self_tokens($tokens);
    $tokens['[' . $this->options['id'] . '-product_id]'] = $this->definition['group'] . ': ' . t('Product ID of the SKU');
  }

  function add_self_tokens(&$tokens, $item) {
    parent::add_self_tokens($tokens, $item);
    $product_id = '';
    $sku = $this->view->result[$this->view->row_index]->commerce_stock_record_product_sku;
    if (!empty($sku)) {
      $product = commerce_product_load_by_sku($sku);
      $product_id = $product->product_id;
    }
    $tokens['[' . $this->options['id'] . '-product_id]'] = $product_id;
  }
}

