<?php

/**
 * @file
 * This contains the views we use for constructing the ajax form columns.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_stock_ajax_add_views_default_views() {
  // (Export start)
$view = new view;
$view->name = 'commerce_stock_ajax_add';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_product';
$view->human_name = 'Commerce Stock Ajax Add';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Commerce Product: Commerce Stock Record */
$handler->display->display_options['relationships']['commerce_stock_record']['id'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['commerce_stock_record']['table'] = 'commerce_product';
$handler->display->display_options['relationships']['commerce_stock_record']['field'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['commerce_stock_record']['required'] = 0;
/* Field: Commerce Product: Product ID */
$handler->display->display_options['fields']['product_id']['id'] = 'product_id';
$handler->display->display_options['fields']['product_id']['table'] = 'commerce_product';
$handler->display->display_options['fields']['product_id']['field'] = 'product_id';
$handler->display->display_options['fields']['product_id']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['external'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['product_id']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['product_id']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['trim'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['html'] = 0;
$handler->display->display_options['fields']['product_id']['element_label_colon'] = 1;
$handler->display->display_options['fields']['product_id']['element_default_classes'] = 1;
$handler->display->display_options['fields']['product_id']['hide_empty'] = 0;
$handler->display->display_options['fields']['product_id']['empty_zero'] = 0;
$handler->display->display_options['fields']['product_id']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['product_id']['link_to_product'] = 0;
/* Field: Commerce Product: SKU */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['label'] = 'Item SKU';
$handler->display->display_options['fields']['sku']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['sku']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['sku']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['sku']['alter']['external'] = 0;
$handler->display->display_options['fields']['sku']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['sku']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['sku']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['sku']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['sku']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['sku']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['sku']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['sku']['alter']['trim'] = 0;
$handler->display->display_options['fields']['sku']['alter']['html'] = 0;
$handler->display->display_options['fields']['sku']['element_label_colon'] = 1;
$handler->display->display_options['fields']['sku']['element_default_classes'] = 1;
$handler->display->display_options['fields']['sku']['hide_empty'] = 0;
$handler->display->display_options['fields']['sku']['empty_zero'] = 0;
$handler->display->display_options['fields']['sku']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
/* Field: Commerce Product: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'commerce_product';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'Product Type';
$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['type']['alter']['external'] = 0;
$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['type']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
$handler->display->display_options['fields']['type']['alter']['html'] = 0;
$handler->display->display_options['fields']['type']['element_label_colon'] = 1;
$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
$handler->display->display_options['fields']['type']['hide_empty'] = 0;
$handler->display->display_options['fields']['type']['empty_zero'] = 0;
$handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['type']['link_to_product'] = 0;
$handler->display->display_options['fields']['type']['use_raw_value'] = 0;
/* Field: Field: Product Line */
$handler->display->display_options['fields']['field_product_line']['id'] = 'field_product_line';
$handler->display->display_options['fields']['field_product_line']['table'] = 'field_data_field_product_line';
$handler->display->display_options['fields']['field_product_line']['field'] = 'field_product_line';
$handler->display->display_options['fields']['field_product_line']['label'] = 'Line';
$handler->display->display_options['fields']['field_product_line']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_product_line']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_product_line']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_product_line']['element_label_colon'] = 1;
$handler->display->display_options['fields']['field_product_line']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_product_line']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_product_line']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_product_line']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_product_line']['group_rows'] = 1;
$handler->display->display_options['fields']['field_product_line']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_product_line']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_product_line']['delta_first_last'] = 0;
$handler->display->display_options['fields']['field_product_line']['field_api_classes'] = 0;
/* Field: Commerce Product: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Name';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_product'] = 0;
/* Field: Commerce Stock Record: Stock */
$handler->display->display_options['fields']['stock']['id'] = 'stock';
$handler->display->display_options['fields']['stock']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['field'] = 'stock';
$handler->display->display_options['fields']['stock']['relationship'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['label'] = 'Current Stock';
$handler->display->display_options['fields']['stock']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stock']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stock']['alter']['external'] = 0;
$handler->display->display_options['fields']['stock']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stock']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stock']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stock']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stock']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stock']['alter']['html'] = 0;
$handler->display->display_options['fields']['stock']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stock']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stock']['hide_empty'] = 0;
$handler->display->display_options['fields']['stock']['empty_zero'] = 0;
$handler->display->display_options['fields']['stock']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stock']['set_precision'] = 0;
$handler->display->display_options['fields']['stock']['precision'] = '0';
$handler->display->display_options['fields']['stock']['format_plural'] = 0;
/* Contextual filter: Commerce Product: SKU */
$handler->display->display_options['arguments']['sku']['id'] = 'sku';
$handler->display->display_options['arguments']['sku']['table'] = 'commerce_product';
$handler->display->display_options['arguments']['sku']['field'] = 'sku';
$handler->display->display_options['arguments']['sku']['default_action'] = 'empty';
$handler->display->display_options['arguments']['sku']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['sku']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['sku']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['sku']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['sku']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['sku']['glossary'] = 0;
$handler->display->display_options['arguments']['sku']['limit'] = '0';
$handler->display->display_options['arguments']['sku']['transform_dash'] = 0;
$handler->display->display_options['arguments']['sku']['break_phrase'] = 0;
/* Filter criterion: Commerce Product: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'commerce_product';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['operator'] = 'not in';
$handler->display->display_options['filters']['type']['value'] = array(
  'fabric_item_request' => 'fabric_item_request',
);
$view = new view;
$view->name = 'commerce_stock_ajax_add';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_product';
$view->human_name = 'Commerce Stock Ajax Add';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Commerce Product: Commerce Stock Record */
$handler->display->display_options['relationships']['commerce_stock_record']['id'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['commerce_stock_record']['table'] = 'commerce_product';
$handler->display->display_options['relationships']['commerce_stock_record']['field'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['commerce_stock_record']['required'] = 0;
/* Field: Commerce Product: Product ID */
$handler->display->display_options['fields']['product_id']['id'] = 'product_id';
$handler->display->display_options['fields']['product_id']['table'] = 'commerce_product';
$handler->display->display_options['fields']['product_id']['field'] = 'product_id';
$handler->display->display_options['fields']['product_id']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['external'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['product_id']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['product_id']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['trim'] = 0;
$handler->display->display_options['fields']['product_id']['alter']['html'] = 0;
$handler->display->display_options['fields']['product_id']['element_label_colon'] = 1;
$handler->display->display_options['fields']['product_id']['element_default_classes'] = 1;
$handler->display->display_options['fields']['product_id']['hide_empty'] = 0;
$handler->display->display_options['fields']['product_id']['empty_zero'] = 0;
$handler->display->display_options['fields']['product_id']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['product_id']['link_to_product'] = 0;
/* Field: Commerce Product: SKU */
$handler->display->display_options['fields']['sku']['id'] = 'sku';
$handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
$handler->display->display_options['fields']['sku']['field'] = 'sku';
$handler->display->display_options['fields']['sku']['label'] = 'Item SKU';
$handler->display->display_options['fields']['sku']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['sku']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['sku']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['sku']['alter']['external'] = 0;
$handler->display->display_options['fields']['sku']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['sku']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['sku']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['sku']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['sku']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['sku']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['sku']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['sku']['alter']['trim'] = 0;
$handler->display->display_options['fields']['sku']['alter']['html'] = 0;
$handler->display->display_options['fields']['sku']['element_label_colon'] = 1;
$handler->display->display_options['fields']['sku']['element_default_classes'] = 1;
$handler->display->display_options['fields']['sku']['hide_empty'] = 0;
$handler->display->display_options['fields']['sku']['empty_zero'] = 0;
$handler->display->display_options['fields']['sku']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['sku']['link_to_product'] = 0;
/* Field: Commerce Product: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'commerce_product';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = 'Product Type';
$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['type']['alter']['external'] = 0;
$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['type']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
$handler->display->display_options['fields']['type']['alter']['html'] = 0;
$handler->display->display_options['fields']['type']['element_label_colon'] = 1;
$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
$handler->display->display_options['fields']['type']['hide_empty'] = 0;
$handler->display->display_options['fields']['type']['empty_zero'] = 0;
$handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['type']['link_to_product'] = 0;
$handler->display->display_options['fields']['type']['use_raw_value'] = 0;
/* Field: Field: Product Line */
$handler->display->display_options['fields']['field_product_line']['id'] = 'field_product_line';
$handler->display->display_options['fields']['field_product_line']['table'] = 'field_data_field_product_line';
$handler->display->display_options['fields']['field_product_line']['field'] = 'field_product_line';
$handler->display->display_options['fields']['field_product_line']['label'] = 'Line';
$handler->display->display_options['fields']['field_product_line']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_product_line']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_product_line']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_product_line']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_product_line']['element_label_colon'] = 1;
$handler->display->display_options['fields']['field_product_line']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_product_line']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_product_line']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_product_line']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_product_line']['group_rows'] = 1;
$handler->display->display_options['fields']['field_product_line']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_product_line']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_product_line']['delta_first_last'] = 0;
$handler->display->display_options['fields']['field_product_line']['field_api_classes'] = 0;
/* Field: Commerce Product: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Name';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_product'] = 0;
/* Field: Commerce Stock Record: Stock */
$handler->display->display_options['fields']['stock']['id'] = 'stock';
$handler->display->display_options['fields']['stock']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['field'] = 'stock';
$handler->display->display_options['fields']['stock']['relationship'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['label'] = 'Current Stock';
$handler->display->display_options['fields']['stock']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stock']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stock']['alter']['external'] = 0;
$handler->display->display_options['fields']['stock']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stock']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stock']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stock']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stock']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stock']['alter']['html'] = 0;
$handler->display->display_options['fields']['stock']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stock']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stock']['hide_empty'] = 0;
$handler->display->display_options['fields']['stock']['empty_zero'] = 0;
$handler->display->display_options['fields']['stock']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stock']['set_precision'] = 0;
$handler->display->display_options['fields']['stock']['precision'] = '0';
$handler->display->display_options['fields']['stock']['format_plural'] = 0;
/* Contextual filter: Commerce Product: SKU */
$handler->display->display_options['arguments']['sku']['id'] = 'sku';
$handler->display->display_options['arguments']['sku']['table'] = 'commerce_product';
$handler->display->display_options['arguments']['sku']['field'] = 'sku';
$handler->display->display_options['arguments']['sku']['default_action'] = 'empty';
$handler->display->display_options['arguments']['sku']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['sku']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['sku']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['sku']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['sku']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['sku']['glossary'] = 0;
$handler->display->display_options['arguments']['sku']['limit'] = '0';
$handler->display->display_options['arguments']['sku']['transform_dash'] = 0;
$handler->display->display_options['arguments']['sku']['break_phrase'] = 0;
/* Filter criterion: Commerce Product: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'commerce_product';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['operator'] = 'not in';
$handler->display->display_options['filters']['type']['value'] = array(
  'fabric_item_request' => 'fabric_item_request',
);

  // (Export endo)

  $views[$view->name] = $view;
  
  return $views;
}

