<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on commerce_stock_transaction being loaded from the database.
 *
 * This hook is invoked during $commerce_stock_transaction loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $commerce_stock_transaction entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_commerce_stock_transaction_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $commerce_stock_transaction is inserted.
 *
 * This hook is invoked after the $commerce_stock_transaction is inserted into the database.
 *
 * @param CommerceStockRecord $commerce_stock_transaction
 *   The $commerce_stock_transaction that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_commerce_stock_transaction_insert(CommerceStockRecord $commerce_stock_transaction) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('commerce_stock_transaction', $commerce_stock_transaction),
      'extra' => print_r($commerce_stock_transaction, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $commerce_stock_transaction being inserted or updated.
 *
 * This hook is invoked before the $commerce_stock_transaction is saved to the database.
 *
 * @param CommerceStockRecord $commerce_stock_transaction
 *   The $commerce_stock_transaction that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_commerce_stock_transaction_presave(CommerceStockRecord $commerce_stock_transaction) {
  $commerce_stock_transaction->name = 'foo';
}

/**
 * Responds to a $commerce_stock_transaction being updated.
 *
 * This hook is invoked after the $commerce_stock_transaction has been updated in the database.
 *
 * @param CommerceStockRecord $commerce_stock_transaction
 *   The $commerce_stock_transaction that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_commerce_stock_transaction_update(CommerceStockRecord $commerce_stock_transaction) {
  db_update('mytable')
    ->fields(array('extra' => print_r($commerce_stock_transaction, TRUE)))
    ->condition('id', entity_id('commerce_stock_transaction', $commerce_stock_transaction))
    ->execute();
}

/**
 * Responds to $commerce_stock_transaction deletion.
 *
 * This hook is invoked after the $commerce_stock_transaction has been removed from the database.
 *
 * @param CommerceStockRecord $commerce_stock_transaction
 *   The $commerce_stock_transaction that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_commerce_stock_transaction_delete(CommerceStockRecord $commerce_stock_transaction) {
  db_delete('mytable')
    ->condition('pid', entity_id('commerce_stock_transaction', $commerce_stock_transaction))
    ->execute();
}

/**
 * Act on a commerce_stock_transaction that is being assembled before rendering.
 *
 * @param $commerce_stock_transaction
 *   The commerce_stock_transaction entity.
 * @param $view_mode
 *   The view mode the commerce_stock_transaction is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $commerce_stock_transaction->content prior to rendering. The
 * structure of $commerce_stock_transaction->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_commerce_stock_transaction_view($commerce_stock_transaction, $view_mode, $langcode) {
  $commerce_stock_transaction->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for commerce_stock_transactions.
 *
 * @param $build
 *   A renderable array representing the commerce_stock_transaction content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * commerce_stock_transaction content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the commerce_stock_transaction rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_commerce_stock_transaction().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_commerce_stock_transaction_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}


