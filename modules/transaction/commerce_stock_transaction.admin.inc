<?php

/**
 * Add new commerce stock record page callback.
 */
function commerce_stock_transaction_add() {
  $stock_transaction = entity_create('commerce_stock_transaction', array());
  drupal_set_title(t('Create Stock Transaction Entry'));
  if (!empty($line_item_id)) {
    $stock_transaction->line_item_id = $line_item_id;
  }
  $output = drupal_get_form('commerce_stock_transaction_form', $stock_transaction);
  return $output;
}

function commerce_stock_transaction_add_by_stock($stkid) {
  $stock_transaction = entity_create('commerce_stock_transaction', array());
  drupal_set_title(t('Create Stock Transaction Entry'));
  if (!empty($stkid)) {
    $stock_transaction->stkid = $stkid;
  }
  $form = drupal_get_form('commerce_stock_transaction_form', $stock_transaction);

  return $form;
}

/**
 * Stock record Form.
 */
function commerce_stock_transaction_form($form, &$form_state, $stock_transaction = NULL) {
  global $user;

  $stock_transaction->uid = $user->uid;

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $stock_transaction->uid,
  );

  $form['stkid'] = array(
    '#type' => 'textfield',
    '#title' => t('Stock ID'),
    '#description' => t('Enter the stock ID.'),
    //'#autocomplete_path' => 'commerce_stock_record/autocomplete/commerce_stock_record/stkid/commerce_stock_record',
    '#required' => TRUE,
    '#size' => 60,
    '#default_value' => $stock_transaction->stkid,
    '#maxlength' => 255,
    '#weight' => -3,
  );

  // disable stock ID field if there is stock ID
  if (!empty($stock_transaction->stkid)) {
    $form['stkid']['#disabled'] = TRUE;

    // find SKU by stock ID
    $result = db_select('commerce_stock_record', 'c')
      ->fields('c', array('product_sku', 'stock'))
      ->condition('stkid', $stock_transaction->stkid, '=')
      ->execute()
      ->fetchAssoc();

    $stock_transaction->product_sku = $result['product_sku'];
    $current_stock = $result['stock'];
  }

  $form['product_sku'] = array(
    '#type' => 'textfield',
    '#title' => t('Product SKU'),
    '#description' => t('Enter the SKU of the product.'),
    //'#autocomplete_path' => 'commerce_product/autocomplete/commerce_stock_transaction/product_sku/commerce_stock_transaction',
    '#required' => TRUE,
    '#size' => 60,
    '#default_value' => $stock_transaction->product_sku,
    '#maxlength' => 255,
    '#weight' => -2,
  );

  if (!empty($stock_transaction->product_sku)) {
    $form['product_sku']['#disabled'] = TRUE;
  }

  field_attach_form('commerce_stock_transaction', $stock_transaction, $form, $form_state);

  $form['stock'] = array(
    '#type' => 'textfield',
    '#default_value' => $stock_transaction->stock,
    '#title' => t('Stock'),
    '#weight' => -1,
  );

  if (isset($current_stock)) {
    $form['stock']['#description'] = t('Current stock: !stock', array('!stock' => $current_stock));
  }

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save stock transaction entry'),
    '#submit' => $submit + array('commerce_stock_transaction_form_submit'),
  );

  // Show Delete button if we edit stock record.
  $stock_transaction_id = entity_id('commerce_stock_transaction' ,$stock_transaction);
  if (!empty($stock_transaction_id) && commerce_stock_transaction_access('edit', $stock_transaction)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('commerce_stock_transaction_form_submit_delete'),
    );
  }


  $form['#validate'][] = 'commerce_stock_transaction_form_validate';

  $form_state['stock_transaction'] = $stock_transaction;

  return $form;
}

function commerce_stock_transaction_form_validate($form, &$form_state) {

}

/**
 * Stock record submit handler.
 */
function commerce_stock_transaction_form_submit($form, &$form_state) {
  $stock_transaction = $form_state['stock_transaction'];
  entity_form_submit_build_entity('commerce_stock_transaction', $stock_transaction, $form, $form_state);
  commerce_stock_transaction_save($stock_transaction);

  //$stock_transaction_uri = entity_uri('commerce_stock_transaction', $stock_transaction);
  //$form_state['redirect'] = $stock_transaction_uri['path'];
  $form_state['redirect'] = 'admin/commerce/stock-transaction';

  drupal_set_message(t('Stock record %title saved.', array('%title' => entity_label('commerce_stock_transaction', $stock_transaction))));
}

function commerce_stock_transaction_form_submit_delete($form, &$form_state) {
  $stock_transaction = $form_state['stock_transaction'];
  $stock_transaction_uri = entity_uri('commerce_stock_transaction', $stock_transaction);

  // drupal_goto() always favors $_GET['destination'] over its own $path parameter
  // so we reset the $_GET['destination'] and clear cache here
  unset($_GET['destination']);
  drupal_static_reset('drupal_get_destination');
  drupal_get_destination();

  $form_state['redirect'] = $stock_transaction_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function commerce_stock_transaction_delete_form($form, &$form_state, $stock_transaction) {
  $form_state['stock_transaction'] = $stock_transaction;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['stock_type_id'] = array('#type' => 'value', '#value' => entity_id('commerce_stock_transaction', $stock_transaction));
  $stock_transaction_uri = entity_uri('commerce_stock_transaction', $stock_transaction);
  return confirm_form($form,
    t('Are you sure you want to delete stock record %title?', array('%title' => entity_label('commerce_stock_transaction', $stock_transaction))),
    $stock_transaction_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function commerce_stock_transaction_delete_form_submit($form, &$form_state) {
  $stock_transaction = $form_state['stock_transaction'];
  commerce_stock_transaction_delete($stock_transaction);

  drupal_set_message(t('Stock record %title deleted.', array('%title' => entity_label('commerce_stock_transaction', $stock_transaction))));

  $form_state['redirect'] = 'admin/commerce/stock-transaction';
}
