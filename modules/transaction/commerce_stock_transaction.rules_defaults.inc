<?php

/**
 * @file
 * Default rule configurations for Commerce Stock Transaction.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_stock_transaction_default_rules_configuration() {
  $configs = array();

  $data = '
{ "rules_stock_transaction_create_after_updating_stock_record" : {
    "LABEL" : "Stock Transaction: create transaction after updating stock record",
    "PLUGIN" : "reaction rule",
    "TAGS" : [ "undefined" ],
    "REQUIRES" : [ "commerce_stock_transaction", "rules", "commerce_stock_record" ],
    "ON" : [ "commerce_stock_record_after_adjust" ],
    "DO" : [
      { "commerce_stock_transaction_create_transaction_with_extra" : {
          "commerce_stock_record" : [ "stock-record" ],
          "adjustment" : [ "adjustment" ],
          "extra" : [ "extra" ]
        }
      }
    ]
  }
}';

  $rule = rules_import($data);

  $configs[$rule->name] = $rule;

  return $configs;
}
