<?php

/**
 * @file
 * Rules integration for Commerce Stock Transaction
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_stock_transaction_rules_condition_info() {
  $conditions = array();

  $defaults = array(
    'group' => t('Commerce Stock Transaction'),
  );
  $conditions['commerce_stock_record_enabled_on_product'] = $defaults + array(
    'label' => t('Product has stock transaction enabled for its type'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_stock_transaction_product_enabled',
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_stock_transaction_rules_action_info() {
  $actions = array();

  $defaults = array(
    'group' => t('Commerce Stock Transaction'),
  );
  $actions['commerce_stock_transaction_create_transaction'] = $defaults + array(
    'label' => t('Create a new transaction'),
    'parameter' => array(
      'commerce_stock_record' => array(
        'type' => 'commerce_stock_record',
        'label' => t('Stock Record'),
      ),
      'adjustment' => array(
        'type' => 'decimal',
        'label' => 'Stock Adjustmnet',
      ),
      'line_item_id' => array(
        'type' => 'integer',
        'label' => 'Line Item ID',
        'default value' => NULL,
      ),
    ),
  );
  $actions['commerce_stock_transaction_create_transaction_with_extra'] = $defaults + array(
    'label' => t('Create a new transaction with extra information'),
    'parameter' => array(
      'commerce_stock_record' => array(
        'type' => 'commerce_stock_record',
        'label' => t('Stock Record'),
      ),
      'adjustment' => array(
        'type' => 'decimal',
        'label' => t('Stock Adjustmnet'),
      ),
      'extra' => array(
        'type' => 'list<entity>',
        'label' => t('Extra Entity list'),
        'default value' => array(),
      ),
    ),
  );
  $actions['commerce_stock_transaction_create_transaction_deduct_store'] = $defaults + array(
    'label' => t('Create a new transaction to deduct store'),
    'parameter' => array(
      'commerce_stock_record' => array(
        'type' => 'commerce_stock_record',
        'label' => t('Stock Record'),
      ),
      'deduction' => array(
        'type' => 'decimal',
        'label' => 'Deduction value',
      ),
      'line_item_id' => array(
        'type' => 'integer',
        'label' => 'Line Item ID',
        'default value' => NULL,
      ),
    ),
  );
  $actions['commerce_stock_transaction_create_transaction_by_line_item'] = $defaults + array(
    'label' => t('Create a new transaction by line item'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line Item'),
      ),
      'adjustment' => array(
        'type' => 'decimal',
        'label' => 'Stock Adjustmnet',
      ),
    ),
  );

  return $actions;
}

