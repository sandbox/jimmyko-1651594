<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_stock_transaction_views_default_views() {
  $view = new view;
  $view->name = 'commerce_stock_transactions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'commerce_stock_transaction';
  $view->human_name = 'Commerce Stock Transactions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Commerce Stock Transactions';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view commerce stock transactions';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'line_item_id' => 'line_item_id',
    'stktxid' => 'stktxid',
    'order_id' => 'order_id',
    'product_sku' => 'product_sku',
    'stock' => 'stock',
    'name' => 'name',
    'created' => 'created',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'line_item_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'stktxid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'order_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'product_sku' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'stock' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="<?php print url(\'admin/commerce/stock/add\'); ?>>Add Stock Record</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  $handler->display->display_options['header']['area']['tokenize'] = 1;
  /* Relationship: Commerce Stock Transaction: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['label'] = 'Author';
  $handler->display->display_options['relationships']['uid']['required'] = 0;
  /* Relationship: Commerce Stock Transaction: Line Item ID */
  $handler->display->display_options['relationships']['line_item_id']['id'] = 'line_item_id';
  $handler->display->display_options['relationships']['line_item_id']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['relationships']['line_item_id']['field'] = 'line_item_id';
  $handler->display->display_options['relationships']['line_item_id']['required'] = 0;
  /* Field: Commerce Stock Transaction: Line Item ID */
  $handler->display->display_options['fields']['line_item_id']['id'] = 'line_item_id';
  $handler->display->display_options['fields']['line_item_id']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['fields']['line_item_id']['field'] = 'line_item_id';
  $handler->display->display_options['fields']['line_item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['line_item_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['line_item_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['line_item_id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['line_item_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['line_item_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['line_item_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['line_item_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['line_item_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['line_item_id']['hide_alter_empty'] = 1;
  /* Field: Commerce Stock Transaction: Stock Transaction ID */
  $handler->display->display_options['fields']['stktxid']['id'] = 'stktxid';
  $handler->display->display_options['fields']['stktxid']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['fields']['stktxid']['field'] = 'stktxid';
  $handler->display->display_options['fields']['stktxid']['label'] = 'ID';
  $handler->display->display_options['fields']['stktxid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['stktxid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['stktxid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['stktxid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['stktxid']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['stktxid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['stktxid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['stktxid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['stktxid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['stktxid']['separator'] = '';
  $handler->display->display_options['fields']['stktxid']['format_plural'] = 0;
  /* Field: Commerce Line Item: Order ID */
  $handler->display->display_options['fields']['order_id']['id'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['table'] = 'commerce_line_item';
  $handler->display->display_options['fields']['order_id']['field'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['relationship'] = 'line_item_id';
  $handler->display->display_options['fields']['order_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['path'] = 'admin/commerce/orders/[order_id]';
  $handler->display->display_options['fields']['order_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['order_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['order_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['order_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['order_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['order_id']['hide_alter_empty'] = 1;
  /* Field: Commerce Stock Transaction: SKU */
  $handler->display->display_options['fields']['product_sku']['id'] = 'product_sku';
  $handler->display->display_options['fields']['product_sku']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['fields']['product_sku']['field'] = 'product_sku';
  $handler->display->display_options['fields']['product_sku']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['external'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['product_sku']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['product_sku']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['product_sku']['alter']['html'] = 0;
  $handler->display->display_options['fields']['product_sku']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['product_sku']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['product_sku']['hide_empty'] = 0;
  $handler->display->display_options['fields']['product_sku']['empty_zero'] = 0;
  $handler->display->display_options['fields']['product_sku']['hide_alter_empty'] = 1;
  /* Field: Commerce Stock Transaction: Stock */
  $handler->display->display_options['fields']['stock']['id'] = 'stock';
  $handler->display->display_options['fields']['stock']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['fields']['stock']['field'] = 'stock';
  $handler->display->display_options['fields']['stock']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['external'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['stock']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['stock']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['stock']['alter']['html'] = 0;
  $handler->display->display_options['fields']['stock']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['stock']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['stock']['hide_empty'] = 0;
  $handler->display->display_options['fields']['stock']['empty_zero'] = 0;
  $handler->display->display_options['fields']['stock']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['stock']['set_precision'] = 0;
  $handler->display->display_options['fields']['stock']['precision'] = '0';
  $handler->display->display_options['fields']['stock']['format_plural'] = 0;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['name']['link_to_user'] = 1;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
  $handler->display->display_options['fields']['name']['format_username'] = 1;
  /* Field: Commerce Stock Transaction: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'admin/commerce/stock-transaction/[stktxid]/edit?destination=admin/commerce/stock-transaction';
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
  $handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
  /* Sort criterion: Commerce Stock Transaction: Stock Transaction ID */
  $handler->display->display_options['sorts']['stktxid']['id'] = 'stktxid';
  $handler->display->display_options['sorts']['stktxid']['table'] = 'commerce_stock_transaction';
  $handler->display->display_options['sorts']['stktxid']['field'] = 'stktxid';
  $handler->display->display_options['sorts']['stktxid']['order'] = 'DESC';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/stock-transaction';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Stock transactions';
  $handler->display->display_options['menu']['description'] = 'Product stock record transactions';
  $handler->display->display_options['menu']['weight'] = '11';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Stock Transactions';
  $handler->display->display_options['tab_options']['description'] = 'Manage stock transactions in the store.';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';


  $views['commerce_stock_transactions'] = $view;
  return $views;
}

/**
 * Implements hook_views_default_views_alter()
 */
function commerce_stock_transaction_views_default_views_alter(&$views) {
  if (isset($views['commerce_stock_records'])) {
  }
}
