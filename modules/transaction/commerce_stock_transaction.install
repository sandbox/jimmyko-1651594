<?php

/**
 * Implements hook_uninstall().
 */
function commerce_stock_transaction_uninstall() {
  drupal_uninstall_schema('commerce_stock_transaction');
}

/**
 * Implements hook_schema().
 */
function commerce_stock_transaction_schema() {
  $schema = array();

  $schema['commerce_stock_transaction'] = array(
    'description' => 'The base table for commerce stock transaction entries.',
    'fields' => array(
      'stktxid' => array(
        'description' => 'The primary identifier for the commerce stock transaction entry.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'stkid' => array(
        'description' => 'The stock record that the stock transaction associated with..',
        'type' => 'int',
        'unsigned' => TRUE, 
        'not null' => FALSE,
        'translatable' => FALSE,
      ),
      'line_item_id' => array(
        'description' => 'The line item that the stock transaction associated with.',
        'type' => 'int',
        'unsigned' => TRUE, 
        'not null' => FALSE,
        'translatable' => FALSE,
      ),
      'product_sku' => array(
        'description' => 'The SKU of the product the stock transaction associated with.',
        'type' => 'text',
        'not null' => TRUE,
        'translatable' => FALSE,
      ),
      'stock' => array(
        'description' => 'The QTY of the commerce stock transaction.',
        'type' => 'numeric',
        'size' => 'normal',
        'not null' => TRUE,
        'default' => 0,
        'precision' => 10,
        'scale' => 2,
      ),
      'uid' => array(
        'description' => 'ID of Drupal user creator.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the commerce stock transaction record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the commerce stock transaction record was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('stktxid'),
  );

  return $schema;
}
