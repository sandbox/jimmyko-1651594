<?php

/**
 * Implements hook_views_data_alter().
 */
function commerce_stock_transaction_views_data_alter(&$data) {
  $data['commerce_product']['commerce_stock_transaction'] = array(
    'title' => t('Commerce Stock Transaction'),
    'help' => t('Commerce Stock Transaction Reverse Reference'),
    'real field' => 'sku',
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'label' => t('Commerce Stock Transaction'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_stock_transaction',
      'base field' => 'product_sku',
      'relationship table' => 'commerce_product',
      'relationship field' => 'sku',
    ),
  );
  $data['commerce_stock_record']['commerce_stock_transaction'] = array(
    'title' => t('Commerce Stock Transaction'),
    'help' => t('Commerce Stock Transaction Reverse Reference'),
    'real field' => 'stkid',
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'label' => t('Commerce Stock Transaction'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_stock_transaction',
      'base field' => 'stkid',
      'relationship table' => 'commerce_stock_record',
      'relationship field' => 'stkid',
    ),
  );
  $data['commerce_line_item']['commerce_stock_transaction'] = array(
    'title' => t('Commerce Stock Transaction'),
    'help' => t('Commerce Stock Transaction Reverse Reference'),
    'real field' => 'line_item_id',
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'label' => t('Commerce Stock Transaction'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_stock_transaction',
      'base field' => 'line_item_id',
      'relationship table' => 'commerce_line_item',
      'relationship field' => 'line_item_id',
    ),
  );

}
