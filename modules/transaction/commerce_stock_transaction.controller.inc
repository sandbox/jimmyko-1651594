<?php

class CommerceStockTransactionController extends EntityAPIController {

  public function create(array $values = array()) {
    global $user;
    $values += array(
      'line_item_id' => 0,
      'product_sku' => '',
      'stock' => 0,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    );
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('commerce_stock_transaction', $entity);
    $extra_fields = field_info_extra_fields('commerce_stock_transaction', 'commerce_stock_transaction', 'display');

    $content['author'] = array(
      '#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))),
      '#weight' => $extra_fields['author']['display'][$view_mode]['weight'],
    );

    $line_item_link = '';
    $line_item = commerce_line_item_load($entity->line_item_id);
    if (!empty($line_item)) {
      $order = commerce_order_load($line_item->order_id); 
      $order_uri = commerce_order_ui_order_uri($order);
      $line_item_link = l($line_item->order_id, $order_uri['path']);
    }
    $content['order_id'] = array(
      '#entity_type' => 'commerce_stock_transaction',
      '#bundle' => 'commerce_stock_transaction',
      '#field_name' => 'field_stock_transaction_order_id',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Order'),
      '#items' => array(array('value' => $line_item->order_id)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['line_item_id']['display'][$view_mode]['weight'] - 1,
      0 => array('#markup' => $line_item_link)
    );

    $content['line_item_id'] = array(
      '#entity_type' => 'commerce_stock_transaction',
      '#bundle' => 'commerce_stock_transaction',
      '#field_name' => 'field_stock_transaction_line_item_id',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Line Item'),
      '#items' => array(array('value' => $entity->line_item_id)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['line_item_id']['display'][$view_mode]['weight'],
      0 => array('#markup' => $entity->line_item_id)
    );

    $content['stkid'] = array(
      '#entity_type' => 'commerce_stock_transaction',
      '#bundle' => 'commerce_stock_transaction',
      '#field_name' => 'field_stock_transaction_stkid',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Stock ID'),
      '#items' => array(array('value' => $entity->stkid)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['stkid']['display'][$view_mode]['weight'],
      0 => array('#markup' => $entity->stkid)
    );

    if (!empty($entity->product_sku)) {
      $product = commerce_product_load_by_sku($entity->product_sku);
      $product_uri = ltrim(commerce_product_get_properties($product, array(), 'edit_url'), base_path());
      $product_link = l($entity->product_sku, $product_uri);
    }
    $content['product_sku'] = array(
      '#entity_type' => 'commerce_stock_transaction',
      '#bundle' => 'commerce_stock_transaction',
      '#field_name' => 'field_stock_transaction_product_sku',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Product'),
      '#items' => array(array('value' => $entity->product_sku)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['product_sku']['display'][$view_mode]['weight'],
      0 => array('#markup' => $product_link)
    );

    $content['stock'] = array(
      '#entity_type' => 'commerce_stock_transaction',
      '#bundle' => 'commerce_stock_transaction',
      '#field_name' => 'field_stock_transaction_stock',
      '#label_display' => 'inline',
      '#view_mode' => 'full',
      '#language' => LANGUAGE_NONE,
      '#theme' => 'field',
      '#field_type' => 'text',
      '#title' => t('Stock'),
      '#items' => array(array('value' => $entity->stock)),
      '#formatter' => 'text_default',
      '#weight' => $extra_fields['stock']['display'][$view_mode]['weight'],
      0 => array('#markup' => check_plain($entity->stock))
    );

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

/**
 * Commerce Stock Record class.
 */
class CommerceStockTransaction extends Entity {
  protected function defaultLabel() {
    return $this->stktxid;
  }

  protected function defaultUri() {
    return array('path' => 'admin/commerce/stock-transaction/' . $this->identifier());
  }

  public function save() {
    $this->changed = REQUEST_TIME;
    return parent::save();
  }
}

/**
 * Commerce Stock Record Views Controller class.
 */
class CommerceStockTransactionViewsController extends EntityDefaultViewsController {
  public function views_data() {
    $data = array();
    $data = parent::views_data();
    $data['commerce_stock_transaction']['line_item_id']['relationship'] = array(
      'label' => t('Line Item'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_line_item',
      'base field' => 'line_item_id',
      'relationship field' => 'line_item_id',
    );
    $data['commerce_stock_transaction']['stkid']['relationship'] = array(
      'label' => t('Stock ID'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_stock_record',
      'base field' => 'stkid',
      'relationship field' => 'stkid',
    );
    $data['commerce_stock_transaction']['product_sku']['field']['handler'] = 'commerce_stock_transaction_views_handler_field_product_sku';
    $data['commerce_stock_transaction']['product_sku']['relationship'] = array(
      'label' => t('Product'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_product',
      'base field' => 'sku',
      'relationship field' => 'product_sku',
    );
    return $data;
  }
}

/**
 * Commerce Stock Transaction Views Product SKU field handler class.
 */
class commerce_stock_transaction_views_handler_field_product_sku extends views_handler_field {
  /**
   * Render the field.
   *
   * @param $values
   *   The values retrieved from the database.
   */
  public function render($values) {
    $output = '';
    $sku = $values->commerce_stock_transaction_product_sku;
    if (!empty($sku)) {
      $product = commerce_product_load_by_sku($sku);
      $product_uri = ltrim(commerce_product_get_properties($product, array(), 'edit_url'), base_path());
      $output = l($sku, $product_uri);
    }
    return $output;
  }

  function document_self_tokens(&$tokens) {
    parent::document_self_tokens($tokens);
    $tokens['[' . $this->options['id'] . '-product_id]'] = $this->definition['group'] . ': ' . t('Product ID of the SKU');
  }

  function add_self_tokens(&$tokens, $item) {
    parent::add_self_tokens($tokens, $item);
    $product_id = '';
    $sku = $this->view->result[$this->view->row_index]->commerce_stock_transaction_product_sku;
    if (!empty($sku)) {
      $product = commerce_product_load_by_sku($sku);
      $product_id = $product->product_id;
    }
    $tokens['[' . $this->options['id'] . '-product_id]'] = $product_id;
  }
}

