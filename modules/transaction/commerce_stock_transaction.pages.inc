<?php

/**
 * Commerce Stock Record view callback.
 */
function commerce_stock_transaction_view($stock_transaction) {
  drupal_set_title(entity_label('commerce_stock_transaction', $stock_transaction));
  return entity_view('commerce_stock_transaction', array(entity_id('commerce_stock_transaction', $stock_transaction) => $stock_transaction), 'full');
}
