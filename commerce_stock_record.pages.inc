<?php

/**
 * Commerce Stock Record view callback.
 */
function commerce_stock_record_view($stock_record) {
  drupal_set_title(entity_label('commerce_stock_record', $stock_record));
  return entity_view('commerce_stock_record', array(entity_id('commerce_stock_record', $stock_record) => $stock_record), 'full');
}
