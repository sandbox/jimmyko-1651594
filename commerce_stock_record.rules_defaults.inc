<?php

/**
 * @file
 * Default rule configurations for Commerce Stock Record.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_stock_record_default_rules_configuration() {
  $configs = array();
  if (module_exists('commerce_checkout')) {
    $data = '
{ "rules_update_the_stock_record_on_checkout_completion" : {
    "LABEL" : "Stock Record: Update the stock record on checkout completion",
    "PLUGIN" : "reaction rule",
    "TAGS" : [ "commerce" ],
    "REQUIRES" : [ "commerce_stock_record", "commerce_checkout" ],
    "ON" : [ "commerce_checkout_complete" ],
    "DO" : [
      { "LOOP" : {
          "USING" : { "list" : [ "commerce-order:commerce-line-items" ] },
          "ITEM" : { "list_item" : "Current list item" },
          "DO" : [
            { "commerce_stock_record_decrease_by_line_item" : { "commerce_line_item" : [ "list-item" ] } }
          ]
        }
      }
    ]
  }
}';

    $rule = rules_import($data);
    $configs[$rule->name] = $rule;

    $data = '
{ "rules_rules_stock_record_validate_add_to_cart" : {
    "LABEL" : "Stock Record: validate checkout form",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [ "commerce_stock_record", "rules", "commerce_stock" ],
    "ON" : [ "commerce_stock_check_product_checkout" ],
    "IF" : [
      { "NOT AND" : [
          { "stock_record_enabled_on_product" : { "commerce_product" : [ "commerce_product" ] } },
          { "stock_record_exist_for_product" : { "commerce_product" : [ "commerce_product" ] } },
          { "stock_record_of_product_is_enough" : {
              "commerce_product" : [ "commerce_product" ],
              "qty" : [ "stock-already-ordered" ]
            }
          }
        ]
      }
    ],
    "DO" : [
      { "commerce_stock_checkout_state" : {
          "stock_action" : "1",
          "message" : "The ordered quantity of [commerce-product:sku] exceeds the maximum stock quantity.",
          "approved_quantity" : "0"
        }
      }
    ]
  }
}';

    $rule = rules_import($data);
    $configs[$rule->name] = $rule;
  }
  return $configs;
}
