<?php

/**
 * Export Drupal Commerce products to Views.
 */

/**
 * Implements hook_views_data()
 */
function commerce_stock_record_views_data() {
  $data = array();

  $data['commerce_stock_record']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the stock record.'),
      'handler' => 'commerce_stock_record_handler_field_stock_record_operations',
    ),
  );

  // Expose the stock ID.
  /*$data['commerce_sotck_record']['stkid'] = array(
    'title' => t('Stock ID'),
    'help' => t('The unique internal identifier of the stock.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filer' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
  );*/

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function commerce_stock_record_views_data_alter(&$data) {
  $data['commerce_product']['commerce_stock_record'] = array(
    'title' => t('Commerce Stock Record'),
    'help' => t('Commerce Stock Record Reverse Reference'),
    'real field' => 'sku',
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'relationship' => array(
      'label' => t('Commerce Stock Record'),
      'handler' => 'views_handler_relationship',
      'base' => 'commerce_stock_record',
      'field' => 'sku',
      'base field' => 'product_sku'
    ),
  );
}

