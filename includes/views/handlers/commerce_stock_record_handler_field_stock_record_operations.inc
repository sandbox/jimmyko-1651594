<?php

/**
 * Field handler to present a stock record's operations links.
 */
class commerce_stock_record_handler_field_stock_record_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['stkid'] = 'stkid';
  }

  function allow_advanced_render() {
    return FALSE;
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['add_destination'] = TRUE;

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['add_destination'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add a destination parameter to operations links so users return to this View on form submission.'),
      '#default_value' => $this->options['add_destination'],
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $stkid = $this->get_value($values, 'stkid');

    // Get the operations links.
    $links = menu_contextual_links('commerce-stock-record', 'admin/commerce/stock', array($stkid));

    if (!empty($links)) {
      // Add the destination to the links if specified.
      if ($this->options['add_destination']) {
        foreach ($links as $id => &$link) {
          $link['query'] = drupal_get_destination();
        }
      }

      // Use the same CSS file in Commerce Product module.
      drupal_add_css(drupal_get_path('module', 'commerce_product') . '/theme/commerce_product.admin.css');
      return theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline', 'operations'))));
    }
  }
}
