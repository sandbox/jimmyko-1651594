<?php

/**
 * Implements hook_views_default_views().
 */
function commerce_stock_record_views_default_views() {

  $view = new view;
$view->name = 'commerce_stock_records';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'commerce_stock_record';
$view->human_name = 'Commerce Stock Records';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Commerce Stock Records';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'view commerce stock records';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'stkid' => 'stkid',
  'product_sku' => 'product_sku',
  'title' => 'title',
  'stock' => 'stock',
  'name' => 'name',
  'created' => 'created',
  'operations' => 'operations',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'stkid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'product_sku' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'stock' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'operations' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<a href="<?php print url(\'admin/commerce/stock/add\'); ?>>Add Stock Record</a>';
$handler->display->display_options['header']['area']['format'] = 'full_html';
$handler->display->display_options['header']['area']['tokenize'] = 1;
/* Relationship: Commerce Stock Record: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['relationships']['uid']['label'] = 'Author';
$handler->display->display_options['relationships']['uid']['required'] = 0;
/* Relationship: Commerce Stock Record: SKU */
$handler->display->display_options['relationships']['product_sku']['id'] = 'product_sku';
$handler->display->display_options['relationships']['product_sku']['table'] = 'commerce_stock_record';
$handler->display->display_options['relationships']['product_sku']['field'] = 'product_sku';
$handler->display->display_options['relationships']['product_sku']['required'] = 0;
/* Field: Commerce Stock Record: Stock ID */
$handler->display->display_options['fields']['stkid']['id'] = 'stkid';
$handler->display->display_options['fields']['stkid']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stkid']['field'] = 'stkid';
$handler->display->display_options['fields']['stkid']['label'] = 'ID';
$handler->display->display_options['fields']['stkid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['external'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stkid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stkid']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['html'] = 0;
$handler->display->display_options['fields']['stkid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stkid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stkid']['hide_empty'] = 0;
$handler->display->display_options['fields']['stkid']['empty_zero'] = 0;
$handler->display->display_options['fields']['stkid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stkid']['format_plural'] = 0;
/* Field: Commerce Stock Record: SKU */
$handler->display->display_options['fields']['product_sku']['id'] = 'product_sku';
$handler->display->display_options['fields']['product_sku']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['product_sku']['field'] = 'product_sku';
$handler->display->display_options['fields']['product_sku']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['external'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['product_sku']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['product_sku']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['trim'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['html'] = 0;
$handler->display->display_options['fields']['product_sku']['element_label_colon'] = 1;
$handler->display->display_options['fields']['product_sku']['element_default_classes'] = 1;
$handler->display->display_options['fields']['product_sku']['hide_empty'] = 0;
$handler->display->display_options['fields']['product_sku']['empty_zero'] = 0;
$handler->display->display_options['fields']['product_sku']['hide_alter_empty'] = 1;
/* Field: Commerce Product: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'commerce_product';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'product_sku';
$handler->display->display_options['fields']['title']['label'] = 'Product';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = 1;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_product'] = 1;
/* Field: Commerce Stock Record: Stock */
$handler->display->display_options['fields']['stock']['id'] = 'stock';
$handler->display->display_options['fields']['stock']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['field'] = 'stock';
$handler->display->display_options['fields']['stock']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stock']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stock']['alter']['external'] = 0;
$handler->display->display_options['fields']['stock']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stock']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stock']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stock']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stock']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stock']['alter']['html'] = 0;
$handler->display->display_options['fields']['stock']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stock']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stock']['hide_empty'] = 0;
$handler->display->display_options['fields']['stock']['empty_zero'] = 0;
$handler->display->display_options['fields']['stock']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stock']['set_precision'] = 0;
$handler->display->display_options['fields']['stock']['precision'] = '0';
$handler->display->display_options['fields']['stock']['format_plural'] = 0;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Author';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['name']['link_to_user'] = 1;
$handler->display->display_options['fields']['name']['overwrite_anonymous'] = 0;
$handler->display->display_options['fields']['name']['format_username'] = 1;
/* Field: Commerce Stock Record: Date created */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Created';
$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['created']['alter']['external'] = 0;
$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
$handler->display->display_options['fields']['created']['alter']['html'] = 0;
$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
$handler->display->display_options['fields']['created']['hide_empty'] = 0;
$handler->display->display_options['fields']['created']['empty_zero'] = 0;
$handler->display->display_options['fields']['created']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['created']['date_format'] = 'short';
/* Field: Commerce Stock Record: Operations links */
$handler->display->display_options['fields']['operations']['id'] = 'operations';
$handler->display->display_options['fields']['operations']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['operations']['field'] = 'operations';
$handler->display->display_options['fields']['operations']['label'] = 'Operations';
$handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['operations']['alter']['external'] = 0;
$handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
$handler->display->display_options['fields']['operations']['alter']['html'] = 0;
$handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
$handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
$handler->display->display_options['fields']['operations']['hide_empty'] = 0;
$handler->display->display_options['fields']['operations']['empty_zero'] = 0;
$handler->display->display_options['fields']['operations']['add_destination'] = 1;
/* Sort criterion: Commerce Stock Record: Date created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'commerce_stock_record';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Commerce Stock Record: SKU */
$handler->display->display_options['filters']['product_sku']['id'] = 'product_sku';
$handler->display->display_options['filters']['product_sku']['table'] = 'commerce_stock_record';
$handler->display->display_options['filters']['product_sku']['field'] = 'product_sku';
$handler->display->display_options['filters']['product_sku']['operator'] = 'contains';
$handler->display->display_options['filters']['product_sku']['exposed'] = TRUE;
$handler->display->display_options['filters']['product_sku']['expose']['operator_id'] = 'product_sku_op';
$handler->display->display_options['filters']['product_sku']['expose']['label'] = 'Filtered by SKU (contains)';
$handler->display->display_options['filters']['product_sku']['expose']['operator'] = 'product_sku_op';
$handler->display->display_options['filters']['product_sku']['expose']['identifier'] = 'product_sku';
$handler->display->display_options['filters']['product_sku']['expose']['required'] = 0;
$handler->display->display_options['filters']['product_sku']['expose']['multiple'] = FALSE;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/commerce/stock';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Stock Records';
$handler->display->display_options['menu']['description'] = 'Manage product stock records in the store.';
$handler->display->display_options['menu']['weight'] = '10';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Stock Records';
$handler->display->display_options['tab_options']['description'] = 'Manage stock records in the store.';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'management';

/* Display: Block - Stock records per product */
$handler = $view->new_display('block', 'Block - Stock records per product', 'block_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = '<a href="<?php print url(\'admin/commerce/stock/add/!1\'); ?>>Add Stock Record</a>';
$handler->display->display_options['header']['area']['format'] = 'full_html';
$handler->display->display_options['header']['area']['tokenize'] = 1;
$handler->display->display_options['defaults']['relationships'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Commerce Stock Record: SKU */
$handler->display->display_options['fields']['product_sku']['id'] = 'product_sku';
$handler->display->display_options['fields']['product_sku']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['product_sku']['field'] = 'product_sku';
$handler->display->display_options['fields']['product_sku']['label'] = 'Product ID';
$handler->display->display_options['fields']['product_sku']['exclude'] = TRUE;
$handler->display->display_options['fields']['product_sku']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['product_sku']['alter']['text'] = '[product_sku-product_id]';
$handler->display->display_options['fields']['product_sku']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['external'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['product_sku']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['product_sku']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['trim'] = 0;
$handler->display->display_options['fields']['product_sku']['alter']['html'] = 0;
$handler->display->display_options['fields']['product_sku']['element_label_colon'] = 0;
$handler->display->display_options['fields']['product_sku']['element_default_classes'] = 1;
$handler->display->display_options['fields']['product_sku']['hide_empty'] = 0;
$handler->display->display_options['fields']['product_sku']['empty_zero'] = 0;
$handler->display->display_options['fields']['product_sku']['hide_alter_empty'] = 1;
/* Field: Commerce Stock Record: Stock ID */
$handler->display->display_options['fields']['stkid']['id'] = 'stkid';
$handler->display->display_options['fields']['stkid']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stkid']['field'] = 'stkid';
$handler->display->display_options['fields']['stkid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['external'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stkid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stkid']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stkid']['alter']['html'] = 0;
$handler->display->display_options['fields']['stkid']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stkid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stkid']['hide_empty'] = 0;
$handler->display->display_options['fields']['stkid']['empty_zero'] = 0;
$handler->display->display_options['fields']['stkid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stkid']['separator'] = '';
$handler->display->display_options['fields']['stkid']['format_plural'] = 0;
/* Field: Commerce Stock Record: Stock */
$handler->display->display_options['fields']['stock']['id'] = 'stock';
$handler->display->display_options['fields']['stock']['table'] = 'commerce_stock_record';
$handler->display->display_options['fields']['stock']['field'] = 'stock';
$handler->display->display_options['fields']['stock']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['stock']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['stock']['alter']['external'] = 0;
$handler->display->display_options['fields']['stock']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['stock']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['stock']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['stock']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['stock']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['stock']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['stock']['alter']['trim'] = 0;
$handler->display->display_options['fields']['stock']['alter']['html'] = 0;
$handler->display->display_options['fields']['stock']['element_label_colon'] = 1;
$handler->display->display_options['fields']['stock']['element_default_classes'] = 1;
$handler->display->display_options['fields']['stock']['hide_empty'] = 0;
$handler->display->display_options['fields']['stock']['empty_zero'] = 0;
$handler->display->display_options['fields']['stock']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['stock']['set_precision'] = 0;
$handler->display->display_options['fields']['stock']['precision'] = '0';
$handler->display->display_options['fields']['stock']['format_plural'] = 0;
/* Field: Commerce Product: Operations links */
$handler->display->display_options['fields']['operations']['id'] = 'operations';
$handler->display->display_options['fields']['operations']['table'] = 'commerce_product';
$handler->display->display_options['fields']['operations']['field'] = 'operations';
$handler->display->display_options['fields']['operations']['label'] = 'Operations';
$handler->display->display_options['fields']['operations']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['operations']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['operations']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['operations']['alter']['external'] = 0;
$handler->display->display_options['fields']['operations']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['operations']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['operations']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['operations']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['operations']['alter']['trim'] = 0;
$handler->display->display_options['fields']['operations']['alter']['html'] = 0;
$handler->display->display_options['fields']['operations']['element_label_colon'] = 1;
$handler->display->display_options['fields']['operations']['element_default_classes'] = 1;
$handler->display->display_options['fields']['operations']['hide_empty'] = 0;
$handler->display->display_options['fields']['operations']['empty_zero'] = 0;
$handler->display->display_options['fields']['operations']['add_destination'] = 1;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Commerce Stock Record: SKU */
$handler->display->display_options['arguments']['product_sku']['id'] = 'product_sku';
$handler->display->display_options['arguments']['product_sku']['table'] = 'commerce_stock_record';
$handler->display->display_options['arguments']['product_sku']['field'] = 'product_sku';
$handler->display->display_options['arguments']['product_sku']['default_action'] = 'empty';
$handler->display->display_options['arguments']['product_sku']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['product_sku']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['product_sku']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['product_sku']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['product_sku']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['product_sku']['glossary'] = 0;
$handler->display->display_options['arguments']['product_sku']['limit'] = '0';
$handler->display->display_options['arguments']['product_sku']['transform_dash'] = 0;
$handler->display->display_options['arguments']['product_sku']['break_phrase'] = 0;
$handler->display->display_options['defaults']['filters'] = FALSE;

  $views[$view->name] = $view;
  return $views;
}
